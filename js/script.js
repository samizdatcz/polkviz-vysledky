$(document).ready(function() {
    $('#tabulka').DataTable({
      language: {url: "https://interaktivni.rozhlas.cz/tools/datatables/Czech.json" },
      order: [[ 1, "desc"]],
      searching: false,
      responsive: true,
	  columnDefs: [
            {   
                targets: 0,
                responsivePriority: 1
            },
            {
                targets: 1,
                responsivePriority: 3
            },
            {
                targets: 2,
                responsivePriority: 2
            }
      ]      
    });
} );

Highcharts.setOptions({
    lang: {
        decimalPoint: "," ,
        }
    });

Highcharts.chart('ideograf', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Nejčastější ideologie'
    },
    subtitle: {
        text: '<a href="http://rozhl.as/polkviz">rozhl.as/polkviz</a>'
    },
    credits: {
        enabled: false
    },    
    xAxis: {
        categories: [
                    'Liberalismus',
                    'Centrismus',
                    'Sociální liberalismus',
                    'Neoliberalismus',     
                    'Liberální socialismus',                                   
                    'Klasický liberalismus',
                    'Ostatní'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Podíl odpovědí:'
        }
    },
    plotOptions: {
        column: {
            pointPadding: 0.05,
            borderWidth: 0
        }
    },
    legend: {
            enabled: false
    },
    tooltip: {
        pointFormat: '<b>{point.y:.2f} %</b>'
    },    
    series: [{
        name: 'Odpovědí',
        data: [30.48, 23.10, 19.94, 6.42, 4.37, 2.86, 10.68]

    }]
});