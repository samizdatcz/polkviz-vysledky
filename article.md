title: "Češi jako progresivní liberálové. Podívejte se na výsledky politického kvízu"
perex: "Vyplnili jste politický kvíz 8values na serveru iROZHLAS.cz? Pak nejpravděpodobněji patříte mezi ekonomické centristy a sociální pokrokáře. Vyplývá to z analýzy více než 33 tisíc odpovědí."
authors: ["Michal Zlatkovský"]
published: "6. června 2017"
coverimg: https://upload.wikimedia.org/wikipedia/commons/f/f5/Different_M311s.jpg
coverimg_note: "Foto Tjrasanen/Wikimedia Commons (CC BY-SA 4.0)"
styles: ["//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css","https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css"]
libraries: ["https://unpkg.com/jquery@3.2.1","//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js","https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js","https://code.highcharts.com/highcharts.js"]
options: "" #wide
---

Českou verzi kvízu, který podle sedmdesáti otázek [odhaduje politickou orientaci](https://www.irozhlas.cz/zpravy-domov/jste-konzervativec-socialista-nebo-centrista-otestujte-se-v-politickem-kvizu_1706050647_zlo), si čtenáři zobrazili skoro stotisíckrát. Vyčištěním dat z testů vyplněných mezi pondělním a úterním dopolednem jsme získali celkem 33 133 kvízových výsledků. Všechna data jsou anonymní a nelze je propojit s konkrétními uživateli.

Nejčastěji, ve více než 30 procentech případů, určil kvíz jako politickou orientaci liberalismus. Tomu odpovídá i průměrný výsledek po jednotlivých osách: ekonomicky ve středu, umírněnost v otázkách mezinárodní politiky i občanských svobod a progresivita v sociální oblasti.

<img src="avgres.png" width="100%">

Liberalismus ve výsledcích těsně následuje blízký centrismus. Časté je mezi výsledky sociální a levicové zaměření, ale zastoupení má i neoliberalismus a pravicové ideologie. Více než stokrát ale vyšla v testu i autokracie nebo libertariánský kapitalismus.

<table id="tabulka" class="display">
<thead>
<tr>
<th>Ideologie</th>
<th>Počet výsledků</th>
<th>Podíl z celku</th>
</tr>
</thead>
<tbody>
<tr>
<td>Liberalismus</td>
<td>10098</td>
<td>30,48 %</td>
</tr>
<tr>
<td>Centrismus</td>
<td>7652</td>
<td>23,10 %</td>
</tr>
<tr>
<td>Sociální liberalismus</td>
<td>6607</td>
<td>19,94 %</td>
</tr>
<tr>
<td>Sociální libertarianismus</td>
<td>2127</td>
<td>6,42 %</td>
</tr>
<tr>
<td>Neoliberalismus</td>
<td>1449</td>
<td>4,37 %</td>
</tr>
<tr>
<td>Liberální socialismus</td>
<td>946</td>
<td>2,86 %</td>
</tr>
<tr>
<td>Klasický liberalismus</td>
<td>714</td>
<td>2,16 %</td>
</tr>
<tr>
<td>Libertarianismus</td>
<td>580</td>
<td>1,75 %</td>
</tr>
<tr>
<td>Demokratický socialismus</td>
<td>433</td>
<td>1,31 %</td>
</tr>
<tr>
<td>Pravicový populismus</td>
<td>388</td>
<td>1,17 %</td>
</tr>
<tr>
<td>Umírněný konzervatismus</td>
<td>374</td>
<td>1,13 %</td>
</tr>
<tr>
<td>Levicový populismus</td>
<td>273</td>
<td>0,82 %</td>
</tr>
<tr>
<td>Libertariánský kapitalismus</td>
<td>271</td>
<td>0,82 %</td>
</tr>
<tr>
<td>Křesťanská demokracie</td>
<td>229</td>
<td>0,69 %</td>
</tr>
<tr>
<td>Autokracie</td>
<td>142</td>
<td>0,43 %</td>
</tr>
<tr>
<td>Teokratický distributismus</td>
<td>125</td>
<td>0,38 %</td>
</tr>
<tr>
<td>Progresivismus</td>
<td>117</td>
<td>0,35 %</td>
</tr>
<tr>
<td>Neokonzervatismus</td>
<td>95</td>
<td>0,29 %</td>
</tr>
<tr>
<td>Revoluční socialismus</td>
<td>75</td>
<td>0,23 %</td>
</tr>
<tr>
<td>Státní socialismus</td>
<td>62</td>
<td>0,19 %</td>
</tr>
<tr>
<td>Sociální demokracie</td>
<td>56</td>
<td>0,17 %</td>
</tr>
<tr>
<td>Kapitalistický fašismus</td>
<td>52</td>
<td>0,16 %</td>
</tr>
<tr>
<td>Konzervatismus</td>
<td>48</td>
<td>0,14 %</td>
</tr>
<tr>
<td>Minarchismus</td>
<td>37</td>
<td>0,11 %</td>
</tr>
<tr>
<td>Fašismus</td>
<td>36</td>
<td>0,11 %</td>
</tr>
<tr>
<td>Liberální komunismus</td>
<td>27</td>
<td>0,08 %</td>
</tr>
<tr>
<td>Teokratický socialismus</td>
<td>25</td>
<td>0,08 %</td>
</tr>
<tr>
<td>Autoritářský kapitalismus</td>
<td>21</td>
<td>0,06 %</td>
</tr>
<tr>
<td>Státní kapitalismus</td>
<td>17</td>
<td>0,05 %</td>
</tr>
<tr>
<td>Distributismus</td>
<td>15</td>
<td>0,05 %</td>
</tr>
<tr>
<td>Nacismus</td>
<td>12</td>
<td>0,04 %</td>
</tr>
<tr>
<td>Národní totalitarismus</td>
<td>8</td>
<td>0,02 %</td>
</tr>
<tr>
<td>Marxismus</td>
<td>6</td>
<td>0,02 %</td>
</tr>
<tr>
<td>Stalinismus/Maoismus</td>
<td>6</td>
<td>0,02 %</td>
</tr>
<tr>
<td>Náboženský socialismus</td>
<td>3</td>
<td>0,01 %</td>
</tr>
<tr>
<td>Náboženský komunismus</td>
<td>2</td>
<td>0,01 %</td>
</tr>
<tr>
<td>Anarchokomunismus</td>
<td>1</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Leninismus</td>
<td>1</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Fundamentalismus</td>
<td>1</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Anarchokapitalismus</td>
<td>1</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Trockismus</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>De leonismus</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Anarchosyndikalismus</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Anarchomutualismus</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Globální totalitarismus</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Technokracie</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Náboženský anarchismus</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Reakcionářství</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Anarchoindividualismus</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Tržní anarchismus</td>
<td>0</td>
<td>0,00 %</td>
</tr>
<tr>
<td>Totalitární kapitalismus</td>
<td>0</td>
<td>0,00 %</td>
</tr>
</tbody></table>
<br>
Ojedinělé výsledky kvízu - například 36 odhadů „fašismus“ - mohou být dány i tím, že čtenáři s možnostmi testu experimentovali a neodpovídali podle svého skutečného přesvědčení. U souhrnných výsledků, které na první pohled naznačují příklon k liberalismu, je také nutné brát v potaz to, že se kvíz šíří především prostřednictvím Facebooku mezi spíše mladšími čtenáři z měst. V žádném případě tak nejde o reprezentativní vzorek celého Česka.

<div id="ideograf"></div>

Někteří čtenáři si také už dříve vyplnili [anglickou verzi kvízu](https://8values.github.io/) a své „české“ výsledky později srovnalí s těmi původními. V některých případech byly rozdíly minimální, jindy se ale výsledky lišily výrazněji. To může být dáno i jiným jazykovým kontextem, jak na Facebooku [podotkl](https://www.facebook.com/marek.svehla.9/posts/1738366449514562) redaktor týdeníku Respekt Marek Švehla. „V češtině jsem výrazně větší pokrokář, liberál a internacionalista, v angličtině se držím u středu. Na jazyku tedy evidentně záleží a při slovech jako národ nebo národní hodnoty člověku v češtině pořád ještě naskakuje Emanuel Moravec nebo Gustáv Husák.“

Nehledě na konkrétní výsledky ovšem platí, že kvíz si neklade žádné nároky na politologickou přesnost. Určování ideologie je založeno pouze na algoritmickém odhadu autorů a skutečná politická orientace uživatele se může od výsledků testu i výrazně lišit. Spoluautor [volebních kalkulaček](http://volebnikalkulacka.cz/) sdružení KohoVolit.eu Michal Škop už původní anglické verzi kvízu kritizoval jako „neseriózní hračku, a nikoli seriózní test“.

_Ještě jste si kvíz nezkoušeli? Zjistěte svou pozici na politickém spektru rovnou:_
<iframe style="width: 200%; height: 1500px; -ms-transform: scale(0.5); -webkit-transform: scale(0.5); transform: scale(0.5); position: relative; left: -50%; margin-top: -370px; margin-bottom: -370px; border: 0;" src="https://interaktivni.rozhlas.cz/data/polkviz/index.html"></iframe>